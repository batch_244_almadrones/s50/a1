const coursesData = [
  {
    id: "wdc001",
    name: "PHP-Laravel",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    price: 45000,
    onOffer: true,
    maxSeats: 30,
  },
  {
    id: "wdc002",
    name: "Python - Django",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    price: 50000,
    onOffer: true,
    maxSeats: 20,
  },
  {
    id: "wdc003",
    name: "Java - Springboot",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    price: 47500,
    onOffer: true,
    maxSeats: 25,
  },
];

export default coursesData;
