const bannerData = [
  {
    id: 0,
    title: "Zuitt Coding Boothcamp",
    subtitle: "Opportunities for everyone, everywhere",
    anchorText: "",
    buttonText: "Enroll Now!",
    isHidden: false,
  },
  {
    id: 1,
    title: "Page Not Found",
    subtitle: "Go back to the ",
    anchorText: "Homepage",
    buttonText: "Enroll Now!",
    isHidden: true,
  },
];
export default bannerData;
