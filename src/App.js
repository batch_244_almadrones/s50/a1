import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import AppNavBar from "./components/AppNavBar";
import "./App.css";
import Home from "./pages/Home";
import Courses from "./pages/Course";
import { Container } from "react-bootstrap";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import PageNotFound from "./pages/Error";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserProvider } from "./UserContext";
function App() {
  const [user, setUser] = useState({ email: localStorage.getItem("email") });

  const unsetUser = () => {
    localStorage.clear();
  };

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        {/* <Home /> */}
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
