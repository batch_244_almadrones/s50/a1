import { Button, Row, Col } from "react-bootstrap";
export default function Banner({ bannerProp }) {
  const { title, subtitle, anchorText, buttonText, isHidden } = bannerProp;
  return (
    <Row>
      <Col className="p-5 text-center">
        <h1>{title}</h1>
        <p>
          {subtitle}
          <a href="/">{anchorText}</a>.
        </p>

        <Button variant="primary" hidden={isHidden}>
          {buttonText}
        </Button>
      </Col>
    </Row>
  );
}
