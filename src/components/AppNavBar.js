import { useContext } from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";

export default function AppNavBar() {
  const { user } = useContext(UserContext);
  // const [user, setUser] = useState(localStorage.getItem("email"));
  // console.log(user);

  // function refreshPage() {
  //   window.location.reload();
  // }

  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/">
            Zuitt booking
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={NavLink} to="/">
                Home
              </Nav.Link>
              <Nav.Link as={NavLink} to="/courses">
                Courses
              </Nav.Link>

              {user.email !== null ? (
                <Nav.Link
                  as={NavLink}
                  to="/logout"
                  // onClick={refreshPage}
                >
                  Logout
                </Nav.Link>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/register">
                    Register
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/login">
                    Login
                  </Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      {/* <h1>Hello World!</h1> */}
    </div>
  );
}
