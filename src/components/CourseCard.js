import { useEffect, useState } from "react";
import { Button, Col, Card } from "react-bootstrap";

export default function CourseCard({ courseProp }) {
  // console.log(courseProp);
  const { name, description, price, maxSeats } = courseProp;
  const [count, setCount] = useState(0);
  const [seat, setSeat] = useState(maxSeats);
  const [isOpen, setIsOpen] = useState(false);
  function enroll() {
    //if (seat > 0) {
    setCount(count + 1);
    setSeat(seat - 1);
    // } else if (seat == 0) {
    //   alert("No more seats");
    // }
    console.log(`Enrollees: ${count + 1}`);
    console.log(`Available seats: ${seat - 1}`);
  }

  useEffect(() => {
    if (seat === 0) {
      setIsOpen(true);
      alert("No more seats");
    }
  }, [seat]);

  return (
    <Col>
      <Card className="courseCard p-3 mb-5">
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>Php {price}</Card.Text>
          <Card.Subtitle>Enrollees:</Card.Subtitle>
          <Card.Text>{count} Enrollees</Card.Text>
          <Button variant="primary" onClick={enroll} disabled={isOpen}>
            Enroll
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
}
