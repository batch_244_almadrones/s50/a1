import Banner from "../components/Banner";
import bannerData from "../data/bannerData";
import { Container } from "react-bootstrap";

export default function PageNotFound() {
  const banner = bannerData[1];
  return (
    <>
      <Container>
        <Banner bannerProp={banner} />
      </Container>
    </>
  );
}
