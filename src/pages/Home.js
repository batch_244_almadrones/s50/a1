import Banner from "../components/Banner";
import Higlights from "../components/Highlights";
import bannerData from "../data/bannerData";
import { Container } from "react-bootstrap";
export default function Home() {
  const banner = bannerData[0];
  return (
    <>
      <Container>
        <Banner bannerProp={banner} />
        <Higlights />
      </Container>
    </>
  );
}
