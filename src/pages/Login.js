import { useEffect, useState, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

export default function Register() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  function authenticate(e) {
    // e.preventDefault();

    localStorage.setItem("email", email);
    setUser({ email: localStorage.getItem("email") });
    alert("You are now logged in.");
    setEmail("");
    setPassword("");
    setIsActive("");
  }

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.email !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form className="col-lg-4 xs-12" onSubmit={(e) => authenticate(e)}>
      <Form.Group controlId="userEmail" className="pb-3">
        <h1>Login</h1>
        <Form.Label>Email Address:</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter Email"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="password" className="pb-3">
        <Form.Label>Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter your password."
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Group>

      {isActive ? (
        <Button variant="success" type="submit" id="submitBtn" className="mb-3">
          Submit
        </Button>
      ) : (
        <Button
          variant="secondary"
          type="submit"
          id="submitBtn"
          className="mb-3"
          disabled
        >
          Login
        </Button>
      )}
    </Form>
  );
}
