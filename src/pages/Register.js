import { useContext, useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
export default function Register() {
  const { user } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();
    alert("Thank you for registering!");
    setEmail("");
    setPassword1("");
    setIsActive("");
  }

  useEffect(() => {
    if (
      email !== "" &&
      password1 !== "" &&
      password1 === password2 &&
      password1.length >= 8
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2]);

  return user.email !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form className="col-lg-4 xs-12" onSubmit={(e) => registerUser(e)}>
      <Form.Group controlId="userEmail" className="pb-3">
        <Form.Label>Email Address:</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter Email"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId="password1" className="pb-3">
        <Form.Label>Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter a password, minimum of 8 characters."
          required
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="password2" className="pb-3">
        <Form.Label>Verify Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Please re-enter password"
          required
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn" className="mb-3">
          Submit
        </Button>
      ) : (
        <Button
          variant="danger"
          type="submit"
          id="submitBtn"
          className="mb-3"
          disabled
        >
          Submit
        </Button>
      )}
    </Form>
  );
}
